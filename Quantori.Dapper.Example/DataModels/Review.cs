﻿namespace Quantori.Dapper.Example.DataModels
{
    public class Review : Post
    {
        public string Subject { get; set; }

        public decimal? Mark { get; set; }
    }
}
