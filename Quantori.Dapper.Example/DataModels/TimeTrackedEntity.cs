﻿using System;

namespace Quantori.Dapper.Example.DataModels
{
    public abstract class TimeTrackedEntity
    {
        public DateTimeOffset? CreatedAt { get; set; }
        public DateTimeOffset? LastModifiedAt { get; set; }
    }
}
