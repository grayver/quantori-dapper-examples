﻿namespace Quantori.Dapper.Example.DataModels
{
    public abstract class Post : TimeTrackedEntity
    {
        public int Id { get; set; }

        public string Type { get; set; }

        public int AuthorId { get; set; }

        public string Title { get; set; }

        public string Text { get; set; }

        public int LikeCount { get; set; }


        public User Author { get; set; }
    }
}
