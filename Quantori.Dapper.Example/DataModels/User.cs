﻿using System;
using System.Collections.Generic;

namespace Quantori.Dapper.Example.DataModels
{
    public class User : TimeTrackedEntity
    {
        public int Id { get; set; }

        public string Email { get; set; }

        public bool IsEmailConfirmed { get; set; }

        public string EmailConfirmToken { get; set; }

        public DateTimeOffset? EmailConfirmTokenExpiresAt { get; set; }

        public string Firstname { get; set; }

        public string Lastname { get; set; }

        public DateTimeOffset? RegisteredAt { get; set; }


        public ICollection<Article> AuthoredArticles { get; set; }
        public ICollection<Review> AuthoredReviews { get; set; }
    }
}
