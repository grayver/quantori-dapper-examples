﻿using Dapper;
using Microsoft.Extensions.Configuration;
using Quantori.Dapper.Example.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Quantori.Dapper.Example.Repositories
{
    public class PostRepository : RepositoryBase
    {
        public PostRepository(IConfiguration configuration) : base(configuration)
        {
        }

        public async Task<Article> GetArticleByIdTypedAsync(int id)
        {
            var sql = @"SELECT * FROM Posts WHERE Type = 'article' AND Id = @PostId;";
            var article = await this.DbConnection.QuerySingleOrDefaultAsync<Article>(sql, new { PostId = id });
            return article;
        }

        public async Task<dynamic> GetArticleByIdUntypedAsync(int id)
        {
            var sql = @"SELECT * FROM Posts WHERE Type = 'article' AND Id = @PostId;";
            var article = await this.DbConnection.QuerySingleOrDefaultAsync(sql, new { PostId = id });
            return article;
        }

        public async Task<Article> GetArticleWithAuthorByIdAsync(int id)
        {
            var sql = @"SELECT * FROM Posts P INNER JOIN Users U ON U.Id = P.AuthorId WHERE P.Type = 'article' AND P.Id = @PostId;";
            var articles = await this.DbConnection.QueryAsync<Article, User, Article>(sql, (article, user) =>
            {
                article.Author = user;
                return article;
            }, new { PostId = id }, splitOn: "Id");
            return articles.SingleOrDefault();
        }

        public async Task<List<Post>> GetAuthoredPostsByUserAsyncV1(int userId)
        {
            var sql = @"
    SELECT * FROM Posts WHERE Type = 'article' AND AuthorId = @UserId;
    SELECT * FROM Posts WHERE Type = 'review' AND AuthorId = @UserId;";
            using (var result = await this.DbConnection.QueryMultipleAsync(sql, new { UserId = userId }))
            {
                var articles = await result.ReadAsync<Article>();
                var reviews = await result.ReadAsync<Review>();
                return articles.Cast<Post>().Concat(reviews).ToList();
            }
        }

        public async Task<List<Post>> GetAuthoredPostsByUserAsyncV2(int userId)
        {
            var sql = @"SELECT * FROM Posts WHERE AuthorId = @UserId";
            var posts = new List<Post>();

            using (var reader = await this.DbConnection.ExecuteReaderAsync(sql, new { UserId = userId }))
            {
                var articleParser = reader.GetRowParser<Article>();
                var reviewParser = reader.GetRowParser<Review>();

                while (reader.Read())
                {
                    Post post;
                    switch (reader.GetString(reader.GetOrdinal("Type")))
                    {
                        case "article":
                            post = articleParser(reader);
                            break;
                        case "review":
                            post = reviewParser(reader);
                            break;
                        default:
                            throw new InvalidOperationException();
                    }
                    posts.Add(post);
                }
            }

            return posts;
        }
    }
}
