﻿using Microsoft.Extensions.Configuration;
using System;
using System.Data;
using System.Data.SqlClient;

namespace Quantori.Dapper.Example.Repositories
{
    public abstract class RepositoryBase : IDisposable
    {
        protected IDbConnection DbConnection { get; }

        public RepositoryBase(IConfiguration configuration)
        {
            this.DbConnection = new SqlConnection(configuration.GetConnectionString("Default"));
        }

        public void Dispose()
        {
            this.DbConnection.Dispose();
        }
    }
}
