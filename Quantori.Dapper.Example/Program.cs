﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Quantori.Dapper.Example.DataModels;
using Quantori.Dapper.Example.Repositories;
using Serilog;
using System;
using System.Threading.Tasks;

namespace Quantori.Dapper.Example
{
    class Program
    {
        public static void ConfigureServices(IServiceCollection services, IConfiguration configuration)
        {
            services.AddLogging((builder) =>
            {
                builder.AddSerilog(dispose: true);
            });

            // add custom repos and services
            services.AddScoped<PostRepository>();
        }

        public static async Task<int> Main(string[] args)
        {
            var configBuilder = new ConfigurationBuilder()
                .SetBasePath(AppContext.BaseDirectory);
            configBuilder.AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
            var configuration = configBuilder.Build();

            Log.Logger = new LoggerConfiguration()
                .ReadFrom.Configuration(configuration)
                .CreateLogger();

            var services = new ServiceCollection();
            services.AddSingleton<IConfiguration>(configuration);
            ConfigureServices(services, configuration);
            using (var serviceProvider = services.BuildServiceProvider())
            {
                var postRepo = serviceProvider.GetRequiredService<PostRepository>();
                var logger = serviceProvider.GetRequiredService<ILogger<Program>>();
                
                var post = await postRepo.GetArticleByIdUntypedAsync(1);
                if (post != null)
                {
                    logger.LogInformation("Post found. Id {0} Title {1} Author {2}", (int)post.Id, (string)post.Title, (string)post.Author?.Firstname);
                }
                else
                {
                    logger.LogInformation("Post not found");
                }
                /*
                var posts = await postRepo.GetAuthoredPostsByUserAsyncV2(2);
                foreach (var post in posts)
                {
                    if (post is Article)
                        logger.LogInformation("Article: Id {0} Title {1} Genre {2}", post.Id, post.Title, (post as Article).Genre);
                    else if (post is Review)
                        logger.LogInformation("Review: Id {0} Title {1} Subject {2}", post.Id, post.Title, (post as Review).Subject);
                }
                */
            }

            return 0;
        }
    }
}
